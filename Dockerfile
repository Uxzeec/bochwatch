FROM ruby:2.7.2
RUN apt-get update -qq && apt-get install -y nodejs default-mysql-client
ADD root_ca.crt /usr/local/share/ca-certificates/root_ca.crt
RUN chmod 644 /usr/local/share/ca-certificates/root_ca.crt && update-ca-certificates
WORKDIR /brandforest
COPY Gemfile /brandforest/
COPY Gemfile.lock /brandforest/
RUN bundle install
COPY . /brandforest

EXPOSE 3000

# Configure the main process to run when running the image
CMD ["rails", "server", "-b", "0.0.0.0"]
