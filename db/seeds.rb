# frozen_string_literal: true

require 'faker'


# table brands

brand_attributes = [{
                        title: 'Citizen', bytitle: 'citizen',
                        img: 'abt-2.jpg', description: Faker::Superhero.name
                    },
                    {
                        title: 'Casio', bytitle: 'casio', img: 'abt-1.jpg',
                        description: Faker::Superhero.name
                    },
                    {
                        title: 'Royal London', bytitle: 'royal-london',
                        img: 'abt-3.jpg', description: Faker::Superhero.name
                    },
                    {
                        title: 'Seiko', bytitle: 'seiko', img: 'seiko.png',
                        description: Faker::Superhero.name
                    },
                    {
                        title: 'Diesel', bytitle: 'diesel', img: 'diesel.png',
                        description: Faker::Superhero.name
                    },
                    {
                        title: 'Aerowatch', bytitle: 'aerowatch', img: 'aerowatch.png',
                        description: Faker::Superhero.name
                    },
                    {
                      title: 'Bulova ',bytitle: 'bulova',img: 'bulova.png',
                      description: Faker::Superhero.name
                    },
                    {
                      title: 'Earnshaw', bytitle: 'earnshaw', img: 'earnshaw.png',
                      description: Faker::Superhero.name
                    },
                    {
                      title: 'Orient', bytitle: 'orient', img: 'orient.jpg',
                      description: Faker::Superhero.name
                    },
                    {
                      title: 'Pierre Lannier', bytitle: 'pierrel', img: 'pierrel.jpg',
                      description: Faker::Superhero.name
                    },
                    {
                      title: 'Q&Q', bytitle: 'qandq', img: 'qandq.jpg',
                      description: Faker::Superhero.name
                    },
                    {
                      title: 'Timex', bytitle: 'Timex', img: 'timex.png',
                      description: Faker::Superhero.name
                    },
                    ]

brand_attributes.each do |attr|
  Brand.create(attr) unless Brand.where(attr).first
end

# - - - - -


# table categories

women = Category.create(title: 'Женские', bytitle: 'women', keywords: 'women', description: 'for women')
casio = Category.create(title: 'Casio', bytitle: 'casio', keywords: 'casio', description: 'casio', parent: women)
citizen = Category.create(title: 'Citizen', bytitle: 'citizen', keywords: 'citizen', description: 'citizen', parent: women)
aerowatch = Category.create(title: 'Aerowatch', bytitle: 'aerowatch', keywords: 'aerowatch', description: 'aerowatch', parent: women)
bulova = Category.create(title: 'Bulova', bytitle: 'bulova', keywords: 'bulova', description: 'bulova', parent: women)
earnshaw = Category.create(title:'Earnshaw',bytitle: 'earnshaw',keywords: 'earnshaw',description: 'earnshaw',parent: women)
orient = Category.create(title: 'Orient',bytitle: 'orient',keywords: 'orient',description:'orient',parent:women)
pierrel = Category.create(title: 'Pierre Lannier',bytitle: 'pierrel',keywords:'pierrel',description:'pierrel',parent:women)
royallondon = Category.create(title: 'Royal London',bytitle: 'royallondon',keywords:'royallondon',description:'royallondon',parent:women)

men = Category.create(title: 'Мужские', bytitle: 'men', keywords: 'men', description: 'for men')
casio1 =  Category.create(title: 'Casio', bytitle: 'casio1', keywords: 'casio1', description: 'casio1', parent: women)
citizen1 = Category.create(title: 'Citizen', bytitle: 'citizen1', keywords: 'citizen1', description: 'citizen1', parent: women)
aerowatch1 = Category.create(title: 'Aerowatch', bytitle: 'aerowatch1', keywords: 'aerowatch', description: 'aerowatch', parent: men)
bulova1 = Category.create(title: 'Bulova', bytitle: 'bulova1', keywords: 'bulova', description: 'bulova', parent: men)
earnshaw1 = Category.create(title:'Earnshaw',bytitle: 'earnshaw1',keywords: 'earnshaw',description: 'earnshaw',parent: men)
orient1 = Category.create(title: 'Orient',bytitle: 'orient1',keywords: 'orient',description:'orient',parent:men)
pierrel1 = Category.create(title: 'Pierre Lannier',bytitle: 'pierrel1',keywords:'pierrel1',description:'pierrel1',parent:men)
royallondon1 = Category.create(title: 'Royal London',bytitle: 'royallondon1',keywords:'royallondon1',description:'royallondon1',parent:men)

kids = Category.create(title: 'Детские', bytitle: 'kids', keywords: 'kids', description: 'for kids')
timex = Category.create(title: 'Timex', bytitle: 'timex', keywords: 'timex', description: 'timex', parent: kids)
qandq = Category.create(title: 'Q&Q', bytitle: 'qandq', keywords: 'qandq', description: 'qandq', parent: kids)

# - - - - - - - -


# table products
product_attributes = [
                      {
                          category_id: '2',
                          brand_id: '2',
                          title: 'Casio MQ-24-7BUL',
                          bytitle: 'casio-mq-24-7bul',
                          content: Faker::Lorem.sentence(word_count: 20),
                          price: Faker::Commerce.price,
                          old_price:Faker::Commerce.price,
                          status: 1,
                          keywords: 'keywords',
                          description: Faker::Lorem.sentence(word_count: 10),
                          img: 'p-2.png',
                          hit: 1,
                      },
                      {
                          category_id: '2',
                          brand_id: '2',
                          title: 'Casio GA-1000-1AER',
                          bytitle: 'casio-ga-1000-1aer',
                          content: Faker::Lorem.sentence(word_count: 20),
                          price: Faker::Commerce.price,
                          old_price:Faker::Commerce.price,
                          status: 1,
                          keywords: 'keywords',
                          description: Faker::Lorem.sentence(word_count: 10),
                          img: 'p-3.png',
                          hit: 1,
                      },
                      {
                          category_id: '3',
                          brand_id: '1',
                          title: 'Citizen JP1010-00E',
                          bytitle: 'citizen-jp1010-00e',
                          content: Faker::Lorem.sentence(word_count: 20),
                          price: Faker::Commerce.price,
                          old_price:Faker::Commerce.price,
                          status: 1,
                          keywords: 'keywords',
                          description: Faker::Lorem.sentence(word_count: 10),
                          img: 'p-4.png',
                          hit: 1
                      },
                      {
                          category_id: '3',
                          brand_id: '1',
                          title: 'Citizen BJ2111-08E',
                          bytitle: 'citizen-bj2111-08e',
                          content: Faker::Lorem.sentence(word_count: 20),
                          price: Faker::Commerce.price,
                          old_price:Faker::Commerce.price,
                          status: 1,
                          keywords: 'keywords',
                          description: Faker::Lorem.sentence(word_count: 10),
                          img: 'p-5.png',
                          hit: 1
                      },
                      {
                          category_id: '9',
                          brand_id: '3',
                          title: 'Royal London 41040-01',
                          bytitle: 'royal-london-41040-01',
                          content: Faker::Lorem.sentence(word_count: 20),
                          price: Faker::Commerce.price,
                          old_price:Faker::Commerce.price,
                          status: 1,
                          keywords: 'keywords',
                          description: Faker::Lorem.sentence(word_count: 10),
                          img: 'p-6.png',
                          hit: 1
                      },
                      {
                          category_id: '9',
                          brand_id: '3',
                          title: 'Royal London 20034-02',
                          bytitle: 'royal-london-20034-02',
                          content: Faker::Lorem.sentence(word_count: 20),
                          price: Faker::Commerce.price,
                          old_price:Faker::Commerce.price,
                          status: 1,
                          keywords: 'keywords',
                          description: Faker::Lorem.sentence(word_count: 10),
                          img: 'p-7.png',
                          hit: 1
                      },
                      {
                          category_id: '9',
                          brand_id: '3',
                          title: 'Royal London 41156-02',
                          bytitle: 'royal-london-41156-02',
                          content: Faker::Lorem.sentence(word_count: 20),
                          price: Faker::Commerce.price,
                          old_price:Faker::Commerce.price,
                          status: 1,
                          keywords: 'keywords',
                          description: Faker::Lorem.sentence(word_count: 10),
                          img: 'p-8.png',
                          hit: 1
                      },
                      {
                          category_id: '9',
                          brand_id: '3',
                          title: 'Royal London 6754-99',
                          bytitle: 'royal-london-6754-99',
                          content: Faker::Lorem.sentence(word_count: 20),
                          price: Faker::Commerce.price,
                          old_price:Faker::Commerce.price,
                          status: 1,
                          keywords: 'keywords',
                          description: Faker::Lorem.sentence(word_count: 10),
                          img: 'p-6.png',
                          hit: 1
                      },
                      {
                          category_id: '9',
                          brand_id: '3',
                          title: 'Royal London 12',
                          bytitle: 'royal-london-67512',
                          content: Faker::Lorem.sentence(word_count: 20),
                          price: Faker::Commerce.price,
                          old_price:Faker::Commerce.price,
                          status: 1,
                          keywords: 'keywords',
                          description: Faker::Lorem.sentence(word_count: 10),
                          img: 'p-5.png',
                          hit: 1
                      },
                      {
                          category_id: '9',
                          brand_id: '3',
                          title: 'Royal London 6754-9922',
                          bytitle: 'royal-london-6754-9922',
                          content: Faker::Lorem.sentence(word_count: 20),
                          price: Faker::Commerce.price,
                          old_price:Faker::Commerce.price,
                          status: 1,
                          keywords: 'keywords',
                          description: Faker::Lorem.sentence(word_count: 10),
                          img: 'p-4.png',
                          hit: 1
                      },
                      {
                        category_id: '4',
                        brand_id: '6',
                        title: 'Aerowatch 50931 AA07',
                        bytitle: 'aerowatch-50931-aa07',
                        content: Faker::Lorem.sentence(word_count: 20),
                        price: Faker::Commerce.price,
                        old_price:Faker::Commerce.price,
                        status:1,
                        keywords: 'keywords',
                        description: Faker::Lorem.sentence(word_count: 10),
                        img: 'aerowatch2-1.jpg',
                        hit: 1
                      },
                      {
                        category_id: '4',
                        brand_id: '6',
                        title: 'Aerowatch 60922 AA13',
                        bytitle: 'aerowatch-60922-aa13',
                        content: Faker::Lorem.sentence(word_count: 20),
                        price: Faker::Commerce.price,
                        old_price:Faker::Commerce.price,
                        status:1,
                        keywords: 'keywords',
                        description: Faker::Lorem.sentence(word_count: 10),
                        img: 'aerowatch1-1.jpg',
                        hit: 1
                      },
                      {
                        category_id: '5',
                        brand_id: '7',
                        title: 'Bulova CURV',
                        bytitle: 'bulova-curv',
                        content: Faker::Lorem.sentence(word_count: 20),
                        price: Faker::Commerce.price,
                        old_price:Faker::Commerce.price,
                        status:1,
                        keywords: 'keywords',
                        description: Faker::Lorem.sentence(word_count: 10),
                        img: 'bulova1-1.jpg',
                        hit: 1
                      },
                      {
                        category_id: '5',
                        brand_id: '7',
                        title: 'Bulova Oceanographer',
                        bytitle: 'bulova-oceanographer',
                        content: Faker::Lorem.sentence(word_count: 20),
                        price: Faker::Commerce.price,
                        old_price:Faker::Commerce.price,
                        status:1,
                        keywords: 'keywords',
                        description: Faker::Lorem.sentence(word_count: 10),
                        img: 'bulova2-1.jpg',
                        hit: 1
                      },
                      {
                        category_id: '5',
                        brand_id: '7',
                        title: 'Bulova Precisionist',
                        bytitle: 'bulova-precisionist',
                        content: Faker::Lorem.sentence(word_count: 20),
                        price: Faker::Commerce.price,
                        old_price:Faker::Commerce.price,
                        status:1,
                        keywords: 'keywords',
                        description: Faker::Lorem.sentence(word_count: 10),
                        img: 'bulova3-1.jpg',
                        hit: 1
                      },
                      {
                        category_id: '6',
                        brand_id: '8',
                        title: 'Earnshaw Bauer',
                        bytitle: 'earnshaw-bauer',
                        content: Faker::Lorem.sentence(word_count: 20),
                        price: Faker::Commerce.price,
                        old_price:Faker::Commerce.price,
                        status:1,
                        keywords: 'keywords',
                        description: Faker::Lorem.sentence(word_count: 10),
                        img: 'earnshaw1-1.jpg',
                        hit: 1
                      },
                      {
                        category_id: '6',
                        brand_id: '8',
                        title: 'Earnshaw Cornwall',
                        bytitle: 'earnshaw-cornwall',
                        content: Faker::Lorem.sentence(word_count: 20),
                        price: Faker::Commerce.price,
                        old_price:Faker::Commerce.price,
                        status:1,
                        keywords: 'keywords',
                        description: Faker::Lorem.sentence(word_count: 10),
                        img: 'earnshaw2-1.jpg',
                        hit: 1
                      },
                      {
                        category_id: '6',
                        brand_id: '8',
                        title: 'Earnshaw Longitude',
                        bytitle: 'earnshaw-longitude',
                        content: Faker::Lorem.sentence(word_count: 20),
                        price: Faker::Commerce.price,
                        old_price:Faker::Commerce.price,
                        status:1,
                        keywords: 'keywords',
                        description: Faker::Lorem.sentence(word_count: 10),
                        img: 'earnshaw3-1.jpg',
                        hit: 1
                      },
                      {
                        category_id: '7',
                        brand_id: '9',
                        title: 'Orient Automatic',
                        bytitle: 'orient-automatic',
                        content: Faker::Lorem.sentence(word_count: 20),
                        price: Faker::Commerce.price,
                        old_price:Faker::Commerce.price,
                        status:1,
                        keywords: 'keywords',
                        description: Faker::Lorem.sentence(word_count: 10),
                        img: 'orient1-1.jpg',
                        hit: 1
                      },
                      {
                        category_id: '7',
                        brand_id: '9',
                        title: 'Orient Diving Sport Automatic',
                        bytitle: 'Orient Diving Sport Automatic',
                        content: Faker::Lorem.sentence(word_count: 20),
                        price: Faker::Commerce.price,
                        old_price:Faker::Commerce.price,
                        status:1,
                        keywords: 'keywords',
                        description: Faker::Lorem.sentence(word_count: 10),
                        img: 'orient2-1.jpg',
                        hit: 1
                      },
                      {
                        category_id: '7',
                        brand_id: '9',
                        title: 'Orient Quartz Standart',
                        bytitle: 'orient-quartz-standart',
                        content: Faker::Lorem.sentence(word_count: 20),
                        price: Faker::Commerce.price,
                        old_price:Faker::Commerce.price,
                        status:1,
                        keywords: 'keywords',
                        description: Faker::Lorem.sentence(word_count: 10),
                        img: 'orient3-1.jpg',
                        hit: 1
                      },
                      {
                        category_id: '8',
                        brand_id: '9',
                        title: 'Pierre Lannier Nature',
                        bytitle: 'pierre-lannier-nature',
                        content: Faker::Lorem.sentence(word_count: 20),
                        price: Faker::Commerce.price,
                        old_price:Faker::Commerce.price,
                        status:1,
                        keywords: 'keywords',
                        description: Faker::Lorem.sentence(word_count: 10),
                        img: 'pierre1-1.jpg',
                        hit: 1
                      },
                      {
                        category_id: '8',
                        brand_id: '10',
                        title: 'Pierre Lannier Spirit',
                        bytitle: 'pierre-lannier-Spirit',
                        content: Faker::Lorem.sentence(word_count: 20),
                        price: Faker::Commerce.price,
                        old_price:Faker::Commerce.price,
                        status:1,
                        keywords: 'keywords',
                        description: Faker::Lorem.sentence(word_count: 10),
                        img: 'pierre2-1.jpg',
                        hit: 1
                      },
                      {
                        category_id: '8',
                        brand_id: '10',
                        title: 'Pierre Lannier Weekend Natural',
                        bytitle: 'pierre-lannier-weekend-natural',
                        content: Faker::Lorem.sentence(word_count: 20),
                        price: Faker::Commerce.price,
                        old_price:Faker::Commerce.price,
                        status:1,
                        keywords: 'keywords',
                        description: Faker::Lorem.sentence(word_count: 10),
                        img: 'pierre3-1.jpg',
                        hit: 1
                      },
                      {
                        category_id: '13',
                        brand_id: '6',
                        title: 'Aerowatch 50931 AA01 RN',
                        bytitle: 'aerowatch-50931-aa01-rn',
                        content: Faker::Lorem.sentence(word_count: 20),
                        price: Faker::Commerce.price,
                        old_price:Faker::Commerce.price,
                        status:1,
                        keywords: 'keywords',
                        description: Faker::Lorem.sentence(word_count: 10),
                        img: 'waerowatch1-1.jpg',
                        hit: 1
                      },
                      {
                        category_id: '13',
                        brand_id: '6',
                        title: 'Aerowatch 50931 NO08',
                        bytitle: 'aerowatch-50931-no08',
                        content: Faker::Lorem.sentence(word_count: 20),
                        price: Faker::Commerce.price,
                        old_price:Faker::Commerce.price,
                        status:1,
                        keywords: 'keywords',
                        description: Faker::Lorem.sentence(word_count: 10),
                        img: 'waerowatch2-1.jpg',
                        hit: 1
                      },
                      {
                        category_id: '13',
                        brand_id: '6',
                        title: 'Aerowatch 57981 AA14',
                        bytitle: 'aerowatch-57981-aa14',
                        content: Faker::Lorem.sentence(word_count: 20),
                        price: Faker::Commerce.price,
                        old_price:Faker::Commerce.price,
                        status:1,
                        keywords: 'keywords',
                        description: Faker::Lorem.sentence(word_count: 10),
                        img: 'waerowatch3-1.jpg',
                        hit: 1
                      },
                      {
                        category_id: '14',
                        brand_id: '7',
                        title: 'Bulova Curv 98R239',
                        bytitle: 'bulova-curv-98r239',
                        content: Faker::Lorem.sentence(word_count: 20),
                        price: Faker::Commerce.price,
                        old_price:Faker::Commerce.price,
                        status:1,
                        keywords: 'keywords',
                        description: Faker::Lorem.sentence(word_count: 10),
                        img: 'wbulova1-1.jpg',
                        hit: 1
                      },
                      {
                        category_id: '14',
                        brand_id: '7',
                        title: 'Bulova Marine Star Ladies',
                        bytitle: 'bulova-marine-star-ladies',
                        content: Faker::Lorem.sentence(word_count: 20),
                        price: Faker::Commerce.price,
                        old_price:Faker::Commerce.price,
                        status:1,
                        keywords: 'keywords',
                        description: Faker::Lorem.sentence(word_count: 10),
                        img: 'wbulova2-1.jpg',
                        hit: 1
                      },
                      {
                        category_id: '14',
                        brand_id: '7',
                        title: 'Bulova Milennia',
                        bytitle: 'bulova-milennia',
                        content: Faker::Lorem.sentence(word_count: 20),
                        price: Faker::Commerce.price,
                        old_price:Faker::Commerce.price,
                        status:1,
                        keywords: 'keywords',
                        description: Faker::Lorem.sentence(word_count: 10),
                        img: 'wbulova3-1.jpg',
                        hit: 1
                      },
                      {
                        category_id: '16',
                        brand_id: '9',
                        title: 'Orient Automatic RA-AG0018L10B',
                        bytitle: 'orient-automatic-ra-ag0018l10b',
                        content: Faker::Lorem.sentence(word_count: 20),
                        price: Faker::Commerce.price,
                        old_price:Faker::Commerce.price,
                        status:1,
                        keywords: 'keywords',
                        description: Faker::Lorem.sentence(word_count: 10),
                        img: 'worient1-1.jpg',
                        hit: 1
                      },
                      {
                        category_id: '16',
                        brand_id: '9',
                        title: 'Orient Classic Automatic ER2K001T',
                        bytitle: 'orient-classic-automatic-er2k001t',
                        content: Faker::Lorem.sentence(word_count: 20),
                        price: Faker::Commerce.price,
                        old_price:Faker::Commerce.price,
                        status:1,
                        keywords: 'keywords',
                        description: Faker::Lorem.sentence(word_count: 10),
                        img: 'worient2-1.jpg',
                        hit: 1
                      },
                      {
                        category_id: '16',
                        brand_id: '9',
                        title: 'Orient Dressy Elegant Ladies',
                        bytitle: 'orient-dressy-elegant-ladies',
                        content: Faker::Lorem.sentence(word_count: 20),
                        price: Faker::Commerce.price,
                        old_price:Faker::Commerce.price,
                        status:1,
                        keywords: 'keywords',
                        description: Faker::Lorem.sentence(word_count: 10),
                        img: 'worient3-1.jpg',
                        hit: 1
                      },
                      {
                        category_id: '17',
                        brand_id: '10',
                        title: 'Pierre Lannier Elegance ceramic',
                        bytitle: 'pierre-lannier-elegance-ceramic',
                        content: Faker::Lorem.sentence(word_count: 20),
                        price: Faker::Commerce.price,
                        old_price:Faker::Commerce.price,
                        status:1,
                        keywords: 'keywords',
                        description: Faker::Lorem.sentence(word_count: 10),
                        img: 'wpierre1-1.jpg',
                        hit: 1
                      },
                      {
                        category_id: '17',
                        brand_id: '10',
                        title: 'Pierre Lannier Eolia310F908',
                        bytitle: 'pierre-lannier-eolia310f908',
                        content: Faker::Lorem.sentence(word_count: 20),
                        price: Faker::Commerce.price,
                        old_price:Faker::Commerce.price,
                        status:1,
                        keywords: 'keywords',
                        description: Faker::Lorem.sentence(word_count: 10),
                        img: 'wpierre2-1.jpg',
                        hit: 1
                      },
                      {
                        category_id: '17',
                        brand_id: '10',
                        title: 'Pierre Lannier Multiples',
                        bytitle: 'pierre-lannier-multiples',
                        content: Faker::Lorem.sentence(word_count: 20),
                        price: Faker::Commerce.price,
                        old_price:Faker::Commerce.price,
                        status:1,
                        keywords: 'keywords',
                        description: Faker::Lorem.sentence(word_count: 10),
                        img: 'wpierre3-1.jpg',
                        hit: 1
                      },
                      {
                        category_id: '21',
                        brand_id: '11',
                        title: 'Q&Q VQ13J010Y',
                        bytitle: 'q&q-vq13j010y',
                        content: Faker::Lorem.sentence(word_count: 20),
                        price: Faker::Commerce.price,
                        old_price:Faker::Commerce.price,
                        status:1,
                        keywords: 'keywords',
                        description: Faker::Lorem.sentence(word_count: 10),
                        img: 'qandq1-1.jpg',
                        hit: 1
                      },
                      {
                        category_id: '21',
                        brand_id: '11',
                        title: 'Q&Q VQ13J013Y',
                        bytitle: 'q&q-vq13j013y',
                        content: Faker::Lorem.sentence(word_count: 20),
                        price: Faker::Commerce.price,
                        old_price:Faker::Commerce.price,
                        status:1,
                        keywords: 'keywords',
                        description: Faker::Lorem.sentence(word_count: 10),
                        img: 'qandq2-1.jpg',
                        hit: 1
                      },
                      {
                        category_id: '21',
                        brand_id: '11',
                        title: 'Q&Q VQ96J017Y',
                        bytitle: 'q&q-vq96j017y',
                        content: Faker::Lorem.sentence(word_count: 20),
                        price: Faker::Commerce.price,
                        old_price:Faker::Commerce.price,
                        status:1,
                        keywords: 'keywords',
                        description: Faker::Lorem.sentence(word_count: 10),
                        img: 'qandq3-1.jpg',
                        hit: 1
                      },
                      {
                        category_id: '21',
                        brand_id: '11',
                        title: 'Q&Q VR41J001Y',
                        bytitle: 'q&q-vr41j001y',
                        content: Faker::Lorem.sentence(word_count: 20),
                        price: Faker::Commerce.price,
                        old_price:Faker::Commerce.price,
                        status:1,
                        keywords: 'keywords',
                        description: Faker::Lorem.sentence(word_count: 10),
                        img: 'qandq4-1.jpg',
                        hit: 1
                      },
                      {
                        category_id: '20',
                        brand_id: '12',
                        title: 'Timex TW2T76900VN',
                        bytitle: 'timex-tw2t76900vn',
                        content: Faker::Lorem.sentence(word_count: 20),
                        price: Faker::Commerce.price,
                        old_price:Faker::Commerce.price,
                        status:1,
                        keywords: 'keywords',
                        description: Faker::Lorem.sentence(word_count: 10),
                        img: 'timex1-1.jpg',
                        hit: 1
                      },
                      {
                        category_id: '20',
                        brand_id: '12',
                        title: 'Timex TW2T77000VN',
                        bytitle: 'timex-tw2t77000vn',
                        content: Faker::Lorem.sentence(word_count: 20),
                        price: Faker::Commerce.price,
                        old_price:Faker::Commerce.price,
                        status:1,
                        keywords: 'keywords',
                        description: Faker::Lorem.sentence(word_count: 10),
                        img: 'timex2-1.jpg',
                        hit: 1
                      },
]

product_attributes.each do |attr|
  Product.create(attr) unless Product.where(attr).first
end

# - - - - -

