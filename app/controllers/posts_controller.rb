class PostsController < ApplicationController
  before_action :authenticate_admin!, only: [:index]
  before_action :set_post, only: [:destroy]

  def index
    @posts = Post.all
  end

  def landing
    @post = Post.new
  end

  def create
    @post = Post.new(post_params)
    unless verify_recaptcha
      respond_to do |format|
        if @post.save
          FormMailer.with(form: @post).new_form_email.deliver_later
          format.html { redirect_to '/', notice: 'Post was successfully created.' }
        end
      end
    end
  end

  def destroy
    @post.destroy
    respond_to do |format|
      format.html { redirect_to posts_url, notice: 'Post was successfully destroyed.' }
    end
  end

  private

  def set_post
    @post = Post.find(params[:id])
  end

  def post_params
    params.require(:post).permit(:name, :email, :phone, :message)
  end
end
