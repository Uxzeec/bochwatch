require 'rails_helper'

RSpec.describe "InfoPages", type: :request do
  describe "GET /contact" do
    it "returns http success" do
      get "/info_pages/contact"
      expect(response).to have_http_status(:success)
    end
  end

end
